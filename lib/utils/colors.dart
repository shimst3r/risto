import 'package:flutter/material.dart';

const mediumAquamarineAccent = Color(0xff83CBC4);
const mediumAquamarine = Color(0xff6DAEBF);
const midnightBlue = Color(0xff0A3A6A);
const sandyBrown = Color(0xffF7C23F);
