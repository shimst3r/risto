import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:risto/utils/colors.dart';

class SplashScreen extends StatelessWidget {
  static const String route = 'splash';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mediumAquamarineAccent,
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/checklist-icon.png',
              height: 250.0,
            ),
            Text(
              'Risto',
              style: GoogleFonts.openSans(
                fontSize: 48.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
